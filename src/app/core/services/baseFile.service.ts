import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
  })
export class BaseFileService {
	private fileContent

	public setBaseFile(data) {
		this.fileContent = data
	}
}