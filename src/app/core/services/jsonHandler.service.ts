import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
  })
export class JsonHandlerService {
	private clients: any[]
	private variables: any[]

	public setBaseFile(data) {
		let AllData = JSON.parse(data)
		// console.log(AllData)
		this.clients = AllData.clients
		this.variables = AllData.variables
	}

	public loadClients(): any {
		return this.clients
	}

	public loadVariables(): any {
		return this.variables
	}

	public uploadClient(client, data) {
		const EDITINDEX = this.clients.findIndex(x => x.name == "Cliente 2")
		console.log("clients", this.clients)
		this.clients[EDITINDEX].variables = data
		console.log("clients - editado", this.clients)
	}
}