import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseFileService } from './services/baseFile.service'
import { JsonHandlerService } from './services/jsonHandler.service'

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    BaseFileService,
    JsonHandlerService
  ]
})
export class CoreModule { }
