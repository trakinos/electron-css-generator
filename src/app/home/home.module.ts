import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatDialogModule } from '@angular/material/dialog';

import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './home.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OverlayModule } from '@angular/cdk/overlay';

@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, SharedModule, HomeRoutingModule, OverlayModule, MatDialogModule ],
  providers: [FormsModule, ReactiveFormsModule, MatDialogModule , OverlayModule],
  exports: [ReactiveFormsModule]
})
export class HomeModule {}