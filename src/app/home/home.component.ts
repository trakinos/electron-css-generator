import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { loadavg } from 'os';
import { AddVarComponent } from '../components/add-var/add-var.component';
import { BaseFileService } from '../core/services/baseFile.service';
import { JsonHandlerService } from '../core/services/jsonHandler.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router, private baseFile: BaseFileService, private jsonFile: JsonHandlerService, private formBuilder:FormBuilder, public dialog: MatDialog) { }
  public formGroup: FormGroup;
  result
  file:any;
  variables: any
  clients: any
  loaded:boolean = false
  fullGroup:FormGroup
  group: any = {}
  ngOnInit(): void {

  }
  fileChanged(e) {
      this.file = e.target.files[0];
      this.uploadDocument(e.target.files[0])
      console.log("uploaded")
  }
  jsonChanged(e) {
    this.file = e.target.files[0];
    this.uploadJson(e.target.files[0])
    console.log("uploaded Json")
  }
  uploadDocument(file) {
    let fileReader = new FileReader();
    fileReader.readAsText(file);
    fileReader.onload = (e) => {
      this.baseFile.setBaseFile(fileReader.result)
    }
  }
  uploadJson(file) {
    let fileReader = new FileReader();
    fileReader.readAsText(file);
    fileReader.onload = (e) => {
      this.jsonFile.setBaseFile(fileReader.result)
      this.loadvariables()
    }
  }
  loadvariables() {
    this.variables = this.jsonFile.loadVariables()
    this.clients = this.jsonFile.loadClients()
    console.log("loaded data", this.clients, this.variables)
    this.clients.forEach(element => {
      this.createForm(element)
    });
    this.loaded = true;
  }
  getValue(clientVar:Array<any>, variable:any) {
    let varFinal = clientVar.find(x => x.key == variable.key)
    if(!varFinal) {
      return variable.default
    }
    return varFinal.value;
  }

  createForm(client) {
    // let group = {}
    this.group[client.name] = this.formBuilder.group({})
    this.variables.forEach(element => {
      this.group[client.name].addControl(element.key, this.formBuilder.control(this.getValue(client.variables, element), Validators.required));
    });
  }
  saveItem(client, form) {
    var variables = []
    for (let key of Object.keys(form.value)) {
      var item = {
        key: key,
        value: form.value[key]
      }
      variables.push(item)
      console.log(item);
    }
    this.jsonFile.uploadClient(client, variables)
  }
  newVar() {
    const dialogRef = this.dialog.open(AddVarComponent);
  }
}