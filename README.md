[![Angular Logo](https://www.vectorlogo.zone/logos/angular/angular-icon.svg)](https://angular.io/) [![Electron Logo](https://www.vectorlogo.zone/logos/electronjs/electronjs-icon.svg)](https://electronjs.org/)

Projeto iniciado com  https://github.com/maximegris/angular-electron.git

Mantido por: Ricardo de Aquino

Projeto com o intuito de gerar CSSs para clientes diferentes apartir de um CSS inicial, com variáveis no código.

Feito:
Aplicação do Angular Material
Carregamento de CSS
Carregamento de JSON
Forms dinamicos por 'Cliente'

Todo:
Criação de Variável
Criação de Cliente
Save JSON
Export CSS